package com.epam.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

public class SportCar extends Car implements AutoCloseable {
    private String speedLevel;
    private PrintWriter file;
    private static int numberOfSportCars = 0;

    public SportCar(String brand, String model, String year,
                    String speedLevel) {
        super(brand, model, year);
        this.speedLevel = speedLevel;

        numberOfSportCars++;
        writeCarToFile();
    }

    @Override
    public void writeCarToFile() {
        try {
            file = new PrintWriter(this.getBrand() + " "
                    + this.getModel() + ".txt");
            file.println();
            file.println("Sport car brand: " + this.getBrand());
            file.println("Sport car model: " + this.getModel());
            file.println("Sport car year: " + this.getYear());
            file.println("Sport car speed level: " + this.getSpeedLevel());
        } catch (FileNotFoundException e) {
            System.out.println(e.toString());
        }
    }

    @Override
    public void close() throws Exception {
        if (numberOfSportCars > 1) {
            throw new IOException("Exception in 'close' method of SportCar class.");
        }

        System.out.println("File " + this.getBrand() + " " + this.getModel() + ".txt is closing...");

        if (file != null) {
            file.close();
        } else {
            System.out.println("File is empty.");
        }
    }

    public String getSpeedLevel() {
        return speedLevel;
    }

    public void setSpeedLevel(String speedLevel) {
        this.speedLevel = speedLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SportCar sportCar = (SportCar) o;
        return Objects.equals(speedLevel, sportCar.speedLevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), speedLevel);
    }

    @Override
    public String toString() {
        return "SportCar{" +
                "speedLevel='" + speedLevel + '\'' +
                "} " + super.toString();
    }
}
