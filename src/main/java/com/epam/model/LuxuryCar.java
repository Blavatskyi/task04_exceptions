package com.epam.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

public class LuxuryCar extends Car implements AutoCloseable {
    private String comfortLevel;
    private PrintWriter file;
    private static int numberOfLuxuryCars = 0;


    public LuxuryCar(String brand, String model, String year,
                     String comfortLevel) {
        super(brand, model, year);
        this.comfortLevel = comfortLevel;

        numberOfLuxuryCars++;
        writeCarToFile();
    }


    @Override
    public void writeCarToFile() {
        try {
            file = new PrintWriter(this.getBrand() + " "
                    + this.getModel() + ".txt");
            file.println();
            file.println("Luxury car brand: " + this.getBrand());
            file.println("Luxury car model: " + this.getModel());
            file.println("Luxury car year: " + this.getYear());
            file.println("Luxury car comfort level: " +
                    this.getComfortLevel());
        } catch (FileNotFoundException e) {
            System.out.println(e.toString());
        }
    }

    @Override
    public void close() throws Exception {
        if (numberOfLuxuryCars > 1) {
            throw new IOException("Exception in 'close' method of LuxuryCar class.");
        }
        System.out.println("File " + this.getBrand() + " " + this.getModel() + ".txt is closing...");
        if (file != null) {
            file.close();
        } else {
            System.out.println("File is empty.");
        }
    }

    public String getComfortLevel() {
        return comfortLevel;
    }

    public void setComfortLevel(String comfortLevel) {
        this.comfortLevel = comfortLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LuxuryCar luxuryCar = (LuxuryCar) o;
        return Objects.equals(comfortLevel, luxuryCar.comfortLevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), comfortLevel);
    }

    @Override
    public String toString() {
        return "LuxuryCar{" +
                "comfortLevel='" + comfortLevel + '\'' +
                "} " + super.toString();
    }
}
