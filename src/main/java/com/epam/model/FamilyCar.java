package com.epam.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

public class FamilyCar extends Car implements AutoCloseable {
    private String safetyLevel;
    private PrintWriter file;
    private static int numberOfFamilyCars = 0;

    public FamilyCar(String brand, String model, String year,
                     String safetyLevel) {
        super(brand, model, year);
        this.safetyLevel = safetyLevel;

        numberOfFamilyCars++;
        writeCarToFile();
    }

    @Override
    public void writeCarToFile() {
        try {
            file = new PrintWriter(this.getBrand() + " "
                    + this.getModel() + ".txt");
            file.println();
            file.println("Family car brand: " + this.getBrand());
            file.println("Family car model: " + this.getModel());
            file.println("Family car year: " + this.getYear());
            file.println("Family car safety level: " + this.getSafetyLevel());
        } catch (FileNotFoundException e) {
            System.out.println(e.toString());
        }
    }

    @Override
    public void close() throws Exception {
        if (numberOfFamilyCars > 1) {
            throw new IOException("Exception in 'close' method of FamilyCar class.");
        }
        System.out.println("File " + this.getBrand() + " " + this.getModel() + ".txt is closing...");
        if (file != null) {
            file.close();
        } else {
            System.out.println("File is empty.");
        }
    }

    public String getSafetyLevel() {
        return safetyLevel;
    }

    public void setSafetyLevel(String safetyLevel) {
        this.safetyLevel = safetyLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FamilyCar familyCar = (FamilyCar) o;
        return Objects.equals(safetyLevel, familyCar.safetyLevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), safetyLevel);
    }

    @Override
    public String toString() {
        return "FamilyCar{" +
                "safetyLevel='" + safetyLevel + '\'' +
                "} " + super.toString();
    }
}
