package com.epam.controller;

import com.epam.model.LuxuryCar;

import java.io.IOException;
import java.util.Scanner;

public class LuxuryCarControllerImpl implements Controller {
    private Scanner scanner = new Scanner(System.in);


    @Override
    public void createCar() {
        System.out.println("Enter brand of luxury car: ");
        String brand  = scanner.nextLine();
        System.out.println("Enter model of luxury car: ");
        String model = scanner.nextLine();
        System.out.println("Enter year of luxury car: ");
        String year = scanner.nextLine();
        System.out.println("Enter comfort level of luxury car: ");
        String comfortLevel = scanner.nextLine();

        try(LuxuryCar luxuryCar = new LuxuryCar(brand, model, year, comfortLevel)){
            System.out.println(luxuryCar.getBrand() + " "
                    + luxuryCar.getModel() + ".txt was successfully created.");
        } catch (Exception e){
            e.printStackTrace();
        }

    }
}
