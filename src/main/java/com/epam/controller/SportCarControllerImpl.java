package com.epam.controller;

import com.epam.model.SportCar;

import java.util.Scanner;

public class SportCarControllerImpl implements Controller {
    private Scanner scanner = new Scanner(System.in);

    @Override
    public void createCar() {
        System.out.println("Enter brand of sport car: ");
        String brand = scanner.nextLine();
        System.out.println("Enter model of sport car: ");
        String model = scanner.nextLine();
        System.out.println("Enter year of sport car: ");
        String year = scanner.nextLine();
        System.out.println("Enter speed level of sport car: ");
        String speedLevel = scanner.nextLine();

        try (SportCar sportCar = new SportCar(brand, model, year, speedLevel)) {
            System.out.println(sportCar.getBrand() + " "
                    + sportCar.getModel() + ".txt was successfully created.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
