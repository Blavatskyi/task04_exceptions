package com.epam.controller;

import com.epam.model.FamilyCar;

import java.util.Scanner;

public class FamilyCarControllerImpl implements Controller {
    private Scanner scanner = new Scanner(System.in);

    @Override
    public void createCar() {

        System.out.println("Enter brand of family car: ");
        String brand = scanner.nextLine();

        System.out.println("Enter model of family car: ");
        String model = scanner.nextLine();

        System.out.println("Enter year of family car: ");
        String year = scanner.nextLine();

        System.out.println("Enter safety level of family car: ");
        String safetyLevel = scanner.nextLine();

        try (FamilyCar familyCar = new FamilyCar(brand, model, year,
                safetyLevel)) {
            System.out.println(familyCar.getBrand() + " "
                    + familyCar.getModel() + ".txt was successfully created.");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
