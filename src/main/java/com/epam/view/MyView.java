package com.epam.view;

import com.epam.controller.FamilyCarControllerImpl;
import com.epam.controller.LuxuryCarControllerImpl;
import com.epam.controller.SportCarControllerImpl;

import java.util.Scanner;

public class MyView {
    private Scanner scanner = new Scanner(System.in);
    private FamilyCarControllerImpl familyCar;
    private SportCarControllerImpl sportCar;
    private LuxuryCarControllerImpl luxuryCar;

    public MyView() {
        familyCar = new FamilyCarControllerImpl();
        sportCar = new SportCarControllerImpl();
        luxuryCar = new LuxuryCarControllerImpl();
    }

    private void printMenu() {
        System.out.println("Select: ");
        System.out.println("\t1 - To create family car");
        System.out.println("\t2 - To create sport car");
        System.out.println("\t3 - To create luxury car");
        System.out.println("\tQ - To exit an application");
    }

    private void pressButton1() {
        familyCar.createCar();
    }


    private void pressButton2() {
        sportCar.createCar();
    }


    private void pressButton3() {
        luxuryCar.createCar();
    }

    public void show() {
        String str;
        do {
            printMenu();
            System.out.println("Please, select menu point.");
            str = scanner.nextLine().toUpperCase();
            char key = str.charAt(0);
            if (key == '1') {
                pressButton1();
            } else if (key == '2') {
                pressButton2();
            } else if (key == '3') {
                pressButton3();
            } else {
                System.out.println("Select appropriate action.");
            }
        } while (!str.equals("Q"));
    }
}
